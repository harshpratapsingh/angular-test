
export class UserModel {
  public firstName: string;
  public lastName: string;
  public phoneNumber: string;
  public email: string;
  public dob: string;
  public imagePath: string

  constructor(firstName: string, lastName: string, phoneNumber: string, email: string, dob: string, imagePath: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.dob = dob;
    this.imagePath = imagePath;
  }
}
