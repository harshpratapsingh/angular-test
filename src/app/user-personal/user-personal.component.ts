import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-user-personal',
  templateUrl: './user-personal.component.html',
  styleUrls: ['./user-personal.component.css']
})
export class UserPersonalComponent implements OnInit {
  id: number;
  singleUser: object;

  constructor(private userService: UserService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params.id;
      this.userService.fetchSingleUser(this.id);
    });
    this.userService.singleUserFetcher.subscribe((singleUser: object) => {
      this.singleUser = singleUser;
      console.log(this.singleUser);
    });
  }
}
