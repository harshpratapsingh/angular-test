import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserPersonalComponent } from './user-personal/user-personal.component';
import {RouterModule} from '@angular/router';
import { UserItemComponent } from './user-list/user-item/user-item.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {UserService} from './user.service';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserPersonalComponent,
    UserItemComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
