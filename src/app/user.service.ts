import {UserModel} from './user.model';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
@Injectable()
export class UserService {
  constructor(private http: HttpClient) {
  }

  users: object;
  singleUser: object;
  userFetcher = new Subject();
  singleUserFetcher = new Subject();
  // userConvert: UserModel;
  fetchUsers() {
    this.http.get<object>('https://randomuser.me/api/?results=100').subscribe((response: object) => {
      this.users = response['results'];
      this.userFetcher.next(this.users);
    });
  }

  fetchSingleUser(id) {
    this.http.get<object>(`https://randomuser.me/api/?id=${id}`).subscribe((response: object) => {
      this.singleUser = response['results'];
      this.singleUserFetcher.next(this.singleUser);
       // console.log(this.singleUser);
    });
  }

}
