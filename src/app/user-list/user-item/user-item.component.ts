import {Component, Input, OnInit} from '@angular/core';
import {UserModel} from '../../user.model';
import {UserService} from '../../user.service';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {
  @Input() id: number;
  // @Input() user: object;
  @Input() user: object;
  // @Input() userConvert: UserModel;
  // convertData(user) {
  //   this.userConvert.firstName = user.name.first;
  //   this.userConvert.lastName = user.name.last;
  //   this.userConvert.phoneNumber = user.cell;
  //   this.userConvert.email = user.email;
  //   this.userConvert.dob = user.dob;
  // }
  constructor(private userService: UserService) { }

  ngOnInit() {
  }

}
