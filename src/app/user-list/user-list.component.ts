import { Component, OnInit } from '@angular/core';
import {UserModel} from '../user.model';
import {UserService} from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
   users: object;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.fetchUsers();
    this.userService.userFetcher.subscribe( (user: object) => {
        this.users = user;
      }
    );
  }
}
